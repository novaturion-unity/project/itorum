﻿using UnityEngine;
using TMPro;

namespace Itorum.UI
{
	public class LaunchPanelController : MonoBehaviour
	{
		public TextMeshProUGUI validityText = null;
		public TextMeshProUGUI launchButtonText = null;

		/// <summary>
		/// The color of the text that shows the possibility of missile preparation
		/// </summary>
		public Color validTextColor = new Color(0.5411792f, 1f, 0.2980392f, 1f);
		public Color invalidTextColor = new Color(1f, 0.836674f, 0.2971698f, 1f);

		public bool IsValid { get; set; }
		public bool IsPrepared { get; set; }

		private void Start()
		{
			Debug.Assert(validityText != null, "FireZoneStateText is null");
			Debug.Assert(launchButtonText != null, "LaunchButtonText is null");
		}

		public void SetValidText()
		{
			if (!validityText)
			{ return; }

			validityText.SetText("Самолёт в зоне огня");
			validityText.color = validTextColor;
		}

		public void SetInvalidText()
		{
			if (!validityText)
			{ return; }

			validityText.SetText("Самолёт вне зоны огня");
			validityText.color = invalidTextColor;
		}

		public void SetLaunchText()
		{ launchButtonText?.SetText("Запустить ракету"); }

		public void SetPrepareText()
		{ launchButtonText?.SetText("Подготовить ракету"); }
	}
}
﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Itorum.UI
{
	public class CamerasPanelController : MonoBehaviour
	{
		public List<Button> cameraButtons = null;

		public Button currentCameraButton { get; private set; }

		public ColorBlock defaultButtonColors = ColorBlock.defaultColorBlock;
		public ColorBlock clickedButtonColors = ColorBlock.defaultColorBlock;

		void Start()
		{
			Debug.Assert(cameraButtons != null, "CameraButtons is null");

			if (cameraButtons.Count > 0)
			{
				currentCameraButton = cameraButtons[0];
				currentCameraButton.colors = clickedButtonColors;
			}
		}

		public void SwitchCameraButton(int index)
		{
			if (!currentCameraButton)
			{
				Debug.LogError("CurrentButton is null");
				return;
			}

			if (cameraButtons == null)
			{
				Debug.LogError("CameraButtons list is null");
				return;
			}

			if (index < 0 || index >= cameraButtons.Count)
			{
				Debug.LogError("Index is out of range\nIndex: " + index + "\tRange: [0, " + (cameraButtons.Count - 1) + "]");
				return;
			}

			if (index == cameraButtons.IndexOf(currentCameraButton))
			{ return; }

			currentCameraButton.colors = defaultButtonColors;
			currentCameraButton = cameraButtons[index];
			currentCameraButton.colors = clickedButtonColors;
		}
	}
}
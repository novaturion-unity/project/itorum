using UnityEngine;

namespace Itorum.Managers
{
	public class ActionsManager : MonoBehaviour
	{
		public static ActionsManager Instance { get; private set; }

		public Input.Actions Actions { get; private set; }

		private void Awake()
		{
			if (Instance == null)
			{ Instance = this; }
			else if (Instance == this)
			{ Destroy(this.gameObject); }

			Actions = new Input.Actions();
		}

		private void OnEnable()
		{ Actions.Enable(); }

		private void OnDisable()
		{ Actions.Disable(); }
	}
}

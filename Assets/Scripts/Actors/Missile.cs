﻿using UnityEngine;

namespace Itorum.Actors
{
	[RequireComponent(typeof(Rigidbody))]
	public class Missile : MonoBehaviour
	{
		public float speed = 150.0f;

		/// <summary>
		/// Attached rigidbody
		/// </summary>
		public new Rigidbody rigidbody = null;

		/// <summary>
		/// Attached camera
		/// </summary>
		public new Camera camera = null;

		public GameObject ObjectToHit = null;

		/// <summary>
		/// What objects can be hit by missile
		/// </summary>
		public LayerMask layersToCollide = Physics.DefaultRaycastLayers;

		private Vector3 launchPosition = Vector3.zero;
		/// <summary>
		/// Predicted missile hit position in world
		/// </summary>
		private Vector3 hitPosition = Vector3.zero;

		private Managers.ActorsManager actorsManager = null;

		private void Start()
		{
			Debug.Assert(rigidbody != null, "Rigidbody is null");
			Debug.Assert(camera != null, "Camera is null");

			actorsManager = Managers.ActorsManager.Instance;
		}

		public void Launch()
		{
			if (!rigidbody)
			{ return; }

			launchPosition = this.transform.position;
			hitPosition = CalculateHitPosition();

			this.transform.rotation = rigidbody.rotation = Quaternion.LookRotation(hitPosition);
			rigidbody.velocity = rigidbody.transform.forward * speed;

			Debug.DrawLine(launchPosition, hitPosition, Color.red, 600f);
		}

		/// <summary>
		/// Predict missle explode position in world.
		/// Return target's current position if there are no positive solutions
		/// </summary>
		private Vector3 CalculateHitPosition()
		{
			Vector3 targetRelativePosition = ObjectToHit.transform.position - launchPosition;
			Vector3 targetRelativeVelocity = ObjectToHit.GetComponent<Rigidbody>().velocity - rigidbody.velocity;

			float time = CalculateTimeToHit(targetRelativePosition, targetRelativeVelocity);

			return ObjectToHit.transform.position + targetRelativeVelocity * time;
		}

		private float CalculateTimeToHit(Vector3 targetRelativePosition, Vector3 targetRelativeVelocity)
		{
			float velocitySquared = targetRelativeVelocity.sqrMagnitude;
			if (velocitySquared < 0.001f)
			{ return 0f; }

			// First coefficient in the quadratic equation (ax^2 + bx + c)
			float a = velocitySquared - speed * speed;

			// Handle similar velocities
			if (Mathf.Abs(a) < 0.001f)
			{
				float time = -targetRelativePosition.sqrMagnitude /
					(2f * Vector3.Dot(targetRelativeVelocity, targetRelativePosition));

				// Don't shoot back in time
				return Mathf.Max(time, 0f);
			}

			// Second coefficient in the quadratic equation (ax^2 + bx + c)
			float b = 2f * Vector3.Dot(targetRelativeVelocity, targetRelativePosition);
			// Free term of the quadratic equation (ax^2 + bx + c)
			float c = targetRelativePosition.sqrMagnitude;
			float determinant = b * b - 4f * a * c;

			if (determinant > 0f)
			{
				// Determinant > 0: two intercept paths (most common)
				float firstRoot = (-b + Mathf.Sqrt(determinant)) / (2f * a);
				float secondRoot = (-b - Mathf.Sqrt(determinant)) / (2f * a);

				if (firstRoot > 0f)
				{
					// Both roots are positive
					if (secondRoot > 0f)
					{ return Mathf.Min(firstRoot, secondRoot); }

					// Only firstRoot is positive
					else
					{ return firstRoot; }
				}

				// Don't shoot back in time
				else
				{ return Mathf.Max(secondRoot, 0f); }
			}

			// Determinant < 0: no intercept path
			else if (determinant < 0f)
			{ return 0f; }

			//determinant = 0: one intercept path, pretty much never happens
			else
			// Don't shoot back in time
			{ return Mathf.Max(-b / (2f * a), 0f); }
		}

		private void Hit()
		{
			actorsManager?.OnMissileHitted();
			Destroy(this.gameObject);
		}

		private void OnCollisionEnter(Collision other)
		{
			if ((layersToCollide & (1 << other.gameObject.layer)) != 0)
			{ Hit(); }
		}
	}
}